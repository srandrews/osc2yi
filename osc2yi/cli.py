import os, sys, signal
from time import sleep
from OSC import OSCServer
import socket, struct, logging, types
from collections import deque
import argparse
import platform

from cameras import camera_ips
from controller import Camera

cameras = {}
exit_request = False

# set up logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


# timing for the server
# loops every server_receive_request_timeout seconds
server_receive_request_timeout = 8 

def get_host_ip3():
    from netifaces import interfaces, ifaddresses, AF_INET
    for ifaceName in interfaces():
        addresses = [i['addr'] for i in ifaddresses(ifaceName).setdefault(AF_INET, [{'addr':'No IP addr'}] )]
        logger.debug('{}: {}'.format(ifaceName, ', '.join(addresses)))
        if ifaceName == 'eth0':
            return addresses[0], 10000
        if ifaceName == 'wlan0':
            return addresses[0], 10000
        # rpi on lan
        if ifaceName == 'eth0': 
            return addresses[0], 10000
        # rpi on wlan
        if ifaceName == 'wlan0': 
            return addresses[0], 10000


def get_host_ip2():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP,10000


def get_host_ip(system):
    """
    Platform independent (macos, linux) effort to find host IP.
    This works for a host not on an internet routed LAN
    """

    def get_default_gateway_linux():
        """Read the default gateway directly from /proc."""
        with open('/proc/net/route') as (fh):
            for line in fh:
                fields = line.strip().split()
                if fields[1] != '00000000' or not int(fields[3], 16) & 2:
                    continue
                return socket.inet_ntoa(struct.pack('<L', int(fields[2], 16)))

    port = 10000
    # set IP here if a static one is desired
    # otherwise expectation is a DHCP lease for host MAC
    host_ip = None
    if system == 'Darwin':
        host_ip = socket.gethostbyname(socket.gethostname())
    else:
        if not host_ip:
            local_gateway = get_default_gateway_linux()
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            s.connect((local_gateway, 1))
            host_ip = s.getsockname()[0]
            s.close()
    return (host_ip, port)


def handle_osc(path, tags, args, source):
    global cameras

    logger.info(('OSC Msg {} {} {} {}').format(path, tags, args, source))

    # sample paths
    # /Camera/2/3
    # /All/2/1
    # /[matrix]/[record = 1|stop = 2]/[cam = 1|2|3]
    #
    # /admin/reconnect
    # /admin/restart
    # /admin/reboot
    # /admin/shutdown

    # only on release args[0]
    if args and args[0] == 0:
        osc_msg = path.split('/')[1]
        if osc_msg in ['All', 'Camera']:
            try:
                matrix = path.split('/')[1]
                action = path.split('/')[2]
                camera = path.split('/')[3]
                if matrix == "Camera":
                    if action == "1":
                        # record single camera
                        logger.debug("{} {} record".format(cameras[camera], camera))
                        record(cameras[camera])

                    elif action == "2":
                        # stop single camera
                        logger.debug("{} {} stop".format(cameras[camera], camera))
                        stop(cameras[camera])
                elif matrix == "All":
                    if action == "1":
                        logger.debug("{} record".format(matrix))
                        for i, camera in cameras.iteritems():
                            record(cameras[i])
                    elif action == "2":
                        logger.debug("{} stop".format(matrix))
                        for i, camera in cameras.iteritems():
                            stop(cameras[i])
            except KeyError as ke:
                logger.exception("Exception")
        elif osc_msg in ['admin']:
            action = path.split('/')[2]
            logger.debug(action)
            if action == 'reconnect':
                for i, camera in cameras.iteritems():
                    logger.debug("Camera {}".format(camera))
                    if camera and camera.sock:
                        logger.debug("Camera Socket {}".format(camera.sock))
                        logger.debug("Camera {} close {}".format(i, camera.sock))
                        camera.sock.shutdown(socket.SHUT_RDWR)
                        camera.sock.close()
                for i,camera_ip in camera_ips.iteritems():
                    logger.debug("Reconnect Camera {} {}".format(i,camera_ip))
                    connect_camera(i, camera_ip)

                logger.info("Cameras connected {}".format(cameras))
            if action == 'restart':
                global exit_request
                exit_request = True
            if action == 'reboot':
                os.system("shutdown -r now")
            if action == 'shutdown':
                os.system("shutdown now")


def record(camera):
    if camera:
        camera.do()
        camera.send_start_recording()
    else:
        logger.debug('No camera object for recording')

def stop(camera):
    if camera:
        camera.do()
        camera.send_stop_recording()
    else:
        logger.debug('No camera object for stopping')


def handle_timeout(self):
    #logger.debug('Server receive request timeout')
    pass

def connect_camera(i, camera_ip):
    global cameras
    # Instantiate and connect to a camera
    try:
        cameras[i] = Camera(camera_ip)
        logger.debug("camera socket {}".format(cameras[i].sock))
        logger.debug("Connected {} at {}".format(cameras[i], camera_ip))
    except Exception as e:
        logger.exception(e)
        cameras[i] = None

def main():

    global cameras, exit_request

    parser = argparse.ArgumentParser(description='OSC2YI.')
    parser.add_argument('--nolog', action='store_true', help='Do not log')
    args = parser.parse_args()

    if args.nolog:
        ch.setLevel(logging.CRITICAL)
        os.system('clear')
        os.system('tput civis')

    server = None
    system = platform.system()
    logger.info(('Running on {}').format(system))


    for i,camera_ip in camera_ips.iteritems():
        connect_camera(i, camera_ip)

    logger.info("Cameras connected {}".format(cameras))

    #host_ip, port = get_host_ip(system)
    #host_ip, port = get_host_ip3()
    host_ip, port = ('192.168.1.100', 10000)
    #host_ip, port = get_host_ip2()
    logger.info(('Point OSC client at {} port {}').format(host_ip, port))

    # set up OSC UDPServer
    OSCServer.socket_timeout = server_receive_request_timeout
    server = OSCServer((host_ip, port))
    server.print_tracebacks = True
    server.handle_timeout = types.MethodType(handle_timeout, server)
    server.addMsgHandler('default', handle_osc)

    def signal_handler(sig, frame):
        logger.info('Closing Server')
        os.system('tput cnorm')
        server.close()
        sys.exit(0)

    signal.signal(signal.SIGINT, signal_handler)
    while True:
        for i, camera in cameras.iteritems():
            if camera:
                camera.do()

        server.handle_request()
        if exit_request is True:
            break
    return

if __name__ == '__main__':
    main()
