# bootstrap if we need to
try:
        import setuptools  # noqa
except ImportError:
        from ez_setup import use_setuptools
        use_setuptools()

from setuptools import setup, find_packages

classifiers = []

setup( author = 'Rich Andrews'
     , author_email = 'stuart.r.andrews@gmail.com'
     , classifiers = classifiers
     , description = 'osc2yi'
     , name = 'osc2yi'
     , url = ''
     , packages = find_packages()
     , entry_points = { 'console_scripts': [
              'osc2yi = osc2yi.cli:main'
        ]}
     # there must be nothing on the following line after the = other than a string constant
     , version = '1.0.0'
     , install_requires = [ 
          'pyOSC==0.3.5b5294'
          , 'netifaces==0.10.9'
        ]
     ,dependency_links = [
        ]
     , zip_safe = False
)
